const express = require('express');
const port = 6789;
const app = express();

app.use(express.static("public"));
app.listen(port, () => {
    console.log("Port " + port + " actif")
});