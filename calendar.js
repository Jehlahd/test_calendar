/*function getScript(source, callback) {
    var script = document.createElement('script');
    var prior = document.getElementsByTagName('script')[0];
    script.async = 1;

    script.onload = script.onreadystatechange = function(_, isAbort) {
        if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            script = undefined;

            if(!isAbort) { if(callback) callback()}
        }
    };

    script.src = source;
    prior.parentNode.insertBefore(script, prior);
}*/

document.addEventListener('DOMContentLoaded', () => {
    var calendarEl = document.getElementById('calendar');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ]
    });

    calendar.render();
});

/*document.addEventListener("DOMContentLoaded", () => {
    var calendarEl = document.getElementById("calendar");

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: ['dayGrid', 'interaction', 'timeGrid'],
        defaultView: "dayGridMonth",
        selectable: true,
        selectHelper: true,
        editable: true,
        locale: "fr",
        events: [
            {
                title  : 'event1',
                start  : '2019-10-01'
            },

            {
                title  : 'event2',
                start  : '2019-09-15',
                end    : '2019-09-18'
            },

            {
                title  : 'event3',
                start  : '2019-09-29T12:30:00',
                allDay : false // will make the time show
            }
        ],

        dateClick: function() {
            alert(calendar.events.title)
        }
    });

    calendar.render();
});*/